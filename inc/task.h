#ifndef __TASK_H__
#define __TASK_H__

typedef void os_thread;
typedef os_thread (*os_thread_p)(void *);

#endif // __TASK_H__