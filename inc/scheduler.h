#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include "task.h"

typedef enum {
   task_ready,
   task_running,
   task_blocked,
   task_suspended,
   task_terminated
} task_state;

/* Task control block */
struct tcb { 
   uint32_t *stack; /* Pointer to top of stack */
   task_state state;
   char *name;
   struct tcb *next; /* Pointer to next task in task-list */
   struct tcb *prev; /* Pointer to previous task in task-list */
};

void schedule_task(os_thread_p task, char *task_name, uint32_t *stack, void *arg);

#endif // __SCHEDULER_H__