#define __SYSTICK_CSR  (*(volatile unsigned long*)0xE000E010ul)  /* Control and status register */
#define __SYSTICK_CSR_ENABLE               ((1u) << 0)
#define __SYSTICK_CSR_TICKINT              ((1u) << 1)
#define __SYSTICK_CSR_CLKSOURCE_CPU        ((1u) << 2)
#define __SYSTICK_CSR_CLKSOURCE_REFERENCE  ((0u) << 2)
#define __SYSTICK_CSR_COUNTFLAG            ((1u) << 16)
#define __SYSTICK_RVR  (*(volatile unsigned long*)0xE000E014ul)  /* Reload value register */
#define __SYSTICK_CVR  (*(volatile unsigned long*)0xE000E018ul)  /* Current value register */

#define __ICSR         (*(volatile unsigned long*)0xE000ED04ul)  /* Interrupt Control and Status Register */
#define __ICSR_NMIPENDSET                  ((1u) << 31)
#define __ICSR_PENDSVSET                   ((1u) << 28)
#define __ICSR_PENDSVCLR                   ((1u) << 27)
#define __ICSR_PENDSTSET                   ((1u) << 26)
#define __ICSR_PENDSTCLR                   ((1u) << 25)
#define __ICSR_ISRPENDING                  ((1u) << 22)
#define __ICSR_VECTPENDING_MSK             ((0x3f000ul))
#define __ICSR_VECTACTIVE_MSK              ((0x3ful))
#define __AIRCR        (*(volatile unsigned long*)0xE000ED0Cul)  /* Application Interrupt and Reset Control Register */
#define __SCR          (*(volatile unsigned long*)0xE000ED10ul)  /* System Control Register */
#define __CCR          (*(volatile unsigned long*)0xE000ED14ul)  /* Configuration and Control Register */
#define __SHPR2        (*(volatile unsigned long*)0xE000ED1Cul)  /* System Handler Priority Register 2 */
#define __SHPR2_SVCALL_PRIORITY            ((0xfful) << 24)
#define __SHPR3        (*(volatile unsigned long*)0xE000ED20ul)  /* System Handler Priority Register 3 */
#define __SHPR3_SYSTICK_PRIORITY           ((0xfful) << 24)
#define __SHPR3_PENDSV_PRIORITY            ((0xfful) << 16)