#include <stddef.h>
#include "os.h"
#include "scheduler.h"
#include "task.h"

#define NBR_OF_THREADS  10u

struct context {
   uint32_t r8;
   uint32_t r9;
   uint32_t r10;
   uint32_t r11;
   uint32_t r4;
   uint32_t r5;
   uint32_t r6;
   uint32_t r7;
   uint32_t r0;
   uint32_t r1;
   uint32_t r2;
   uint32_t r3;
   uint32_t r12;
   void (*lr)(void);
   os_thread_p pc;
   uint32_t xpsr;
};

struct exc_context {
   uint32_t r0;
   uint32_t r1;
   uint32_t r2;
   uint32_t r3;
   uint32_t r12;
   uint32_t lr;
   uint32_t pc;
   uint32_t xspr;
};

extern struct tcb background;

struct tcb tcb_pool[NBR_OF_THREADS];
uint8_t tcb_pool_idx;

volatile struct tcb *current_tcb = &background;


static struct tcb *ready_tasks;
static struct tcb *suspended_tasks;
static struct tcb *blocked_tasks;

void thread_exit(void)
{
   while(1);
}

static void init_stack_frame(struct tcb *tcb, os_thread_p task_func) {
   struct context *frm = (struct context *)tcb->stack;
   frm->r0 = 0;
   frm->r1 = 0;
   frm->r2 = 0;
   frm->r3 = 0;
   frm->r4 = 0;
   frm->r5 = 0;
   frm->r6 = 0;
   frm->r7 = 0;
   frm->r8 = 0;
   frm->r9 = 0;
   frm->r10 = 0;
   frm->r11 = 0;
   frm->r12 = 0;
   frm->pc = task_func;
   frm->lr = thread_exit;
   frm->xpsr = 0x01000000;
}

volatile struct context *context = 0x20000f80;
volatile struct exc_context *exc_context = 0x20000f80;

void scheduler(void)
{
   struct tcb *old_tcb = current_tcb;

   if (NULL != ready_tasks)
      current_tcb = ready_tasks;
      if (ready_tasks->next) {
         ready_tasks = ready_tasks->next;
         struct tcb *tmp_ready = ready_tasks;
         while (tmp_ready->next != NULL) {
            tmp_ready = tmp_ready->next;
         }
         tmp_ready->next = old_tcb;
         current_tcb->next = NULL;
      }
}

void schedule_task(os_thread_p task, char *task_name, uint32_t *stack, void *arg)
{
   if (tcb_pool_idx < NBR_OF_THREADS) {
      struct tcb *tcb = &tcb_pool[tcb_pool_idx++];
      tcb->name = task_name;
      tcb->stack = stack - 16;
      init_stack_frame(tcb, task);
      tcb->next = NULL;
      tcb->prev = NULL;
      if (NULL == ready_tasks)
         ready_tasks = tcb;
      else {
         struct tcb *ready_tasks_ptr = ready_tasks;
         while(ready_tasks_ptr->next != NULL) {
            ready_tasks_ptr++;
         }
         ready_tasks_ptr->next = tcb;
         tcb->prev = ready_tasks_ptr;
      }
   }
}