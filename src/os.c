#include "os.h"
#include "cortex-m0.h"

char x = 1;
int test = 2;
int bss;



uint32_t __attribute__((aligned(8))) stack_bg[0x40];
uint32_t __attribute__((aligned(8))) stack_t1[0x40];
uint32_t __attribute__((aligned(8))) stack_t2[0x40];

tcb_t background = {.stack = (uint32_t *)stack_bg+sizeof(stack_bg)/4};
tcb_t task1 = {.stack = (uint32_t *)stack_t1+sizeof(stack_t1)/4};
tcb_t task2 = {.stack = (uint32_t *)stack_t2+sizeof(stack_t2)/4};

void thread_bg(void) {
   int i = 100;
   while(i--);
}

void cyclic_thread(void *arg)
{
   while (1) {
   int i = 100;
   while(i--);
   }
}

void cyclic_thread2(void *arg)
{
   while(1) {
   int y = 0;
   while(y++<100);
   }
}

int os_init(void) {
   // Setup tasks
   //schedule_task(thread_bg, "Background thread", stack_bg + (sizeof(stack_bg)/4u), (void *)0);
   schedule_task(cyclic_thread, "Cyclic thread", (stack_t1) + (sizeof(stack_t1)/4u), (void *)0);
   schedule_task(cyclic_thread2, "Cyclic thread2", stack_t2 + (sizeof(stack_t1)/4u), (void *)0);
   // Setup system tick timer
   __SYSTICK_RVR = 0x1f3fu;
   __SYSTICK_CVR = 0u;
   __SYSTICK_CSR = __SYSTICK_CSR_CLKSOURCE_CPU | __SYSTICK_CSR_ENABLE | __SYSTICK_CSR_TICKINT;
   __SHPR2 = __SHPR2_SVCALL_PRIORITY;
   __SHPR3 = __SHPR3_SYSTICK_PRIORITY | __SHPR3_PENDSV_PRIORITY;
   // Setup background thread
   __asm volatile
   (
      "ldr r0, =background\n"
      "ldr r1, [r0]\n"
      "msr psp, r1\n"
      "movs r0, #2\n"
      "msr CONTROL, r0\n"
      "isb \n"
      "ldr r0, =__StackTop\n"
      "msr msp, r0\n"
      "ldr r0, =thread_bg\n"
      "mov lr, r0\n"
      "cpsie i\n"
      "bx  lr\n"
   );
   return 0;
}

int main(void) {
   (void)x;
   (void)test;
   test = test + x + bss;
   os_init();
   return 0;
}