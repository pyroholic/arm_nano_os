  .file "os_interrupts.s"
  .syntax unified
  .thumb
  .text
  .thumb_func
  .align 1
  .global PendSV_Handler2
  .type PendSV_Handler2, %function
PendSV_Handler2:
  bx lr
  .size PendSV_Handler2, . - PendSV_Handler2
