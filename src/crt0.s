/* Startup for ARM cortex M0 */
  .file "crt0.s"

  .syntax unified
  .arch armv6-m
  .cpu cortex-m0

  .section  .stack
  .align    3
  .equ      Stack_Size, 0xc00
  .global   __StackTop
  .global   __StackLimit
__StackLimit:
  .space Stack_Size
  .size  __StackLimit, . - __StackLimit
__StackTop:
  .size  __StackTop, . - __StackTop

  .section .isr_vector,"a",%progbits
  .align   2
  .global __isr_vector
__isr_vector:
  .long   __StackTop            /* Top of Stack */
  .long   Reset_Handler         /* Reset Handler */
  .long   NMI_Handler           /* NMI Handler */
  .long   HardFault_Handler     /* Hard Fault Handler */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   SVC_Handler           /* SVCall Handler */
  .long   0                     /* Reserved */
  .long   0                     /* Reserved */
  .long   PendSV_Handler        /* PendSV Handler */
  .long   SysTick_Handler       /* SysTick Handler */

  .size __isr_vector, . - __isr_vector

  .text
  .thumb
  .thumb_func
  .align 1
  .global Reset_Handler
  .type Reset_Handler, %function
  .syntax unified
Reset_Handler:
  cpsid i
  ldr   r0, =__data_start__
  ldr   r1, =__data_end__
  ldr   r2, =_etext
  subs  r1, r1, r0
copy_loop:
  subs  r1, r1, #4
  ldr   r3, [r2, r1]
  str   r3, [r0, r1]
  cmp   r1, #0
  bne   copy_loop
  ldr   r0, =__bss_start__
  ldr   r1, =__bss_end__
  ldr   r2, =0
  subs  r1, r0
  beq   clear_done
clear_loop:
  subs  r1, #4
  str   r2, [r0, r1]
  bne   clear_loop
clear_done:
  bl    SystemInit
  bl    _start
  .size Reset_Handler, . - Reset_Handler

  .align 1
  .thumb_func
  .weak Dummy_Handler
  .type Dummy_Handler, %function
Dummy_Handler:
  b     .
  .size Dummy_Handler, . - Dummy_Handler

  .macro  dummy_handler name
  .weak   \name
  .set    \name,  Dummy_Handler
  .endm

  dummy_handler NMI_Handler
  dummy_handler HardFault_Handler
  dummy_handler SVC_Handler
  dummy_handler PendSV_Handler
  dummy_handler SysTick_Handler
  
  .thumb_func
  .weak SystemInit
  .type SystemInit, %function
SystemInit:
  bx  lr
  .size SystemInit, . - SystemInit
_start:
  push {r0,lr}
  bl  main
  pop  {r0,pc}
  .end
