#include <stdint.h>
#include "os.h"
#include "cortex-m0.h"

uint32_t tick_count = 0;

void SysTick_Handler(void)
{
   tick_count++;
   __ICSR = __ICSR_PENDSVSET;
}

void __attribute__((naked)) PendSV_Handler(void) 
{
   __asm volatile
   (
      // Switch context: push the unsaved registers on stack: r4-r7, r8-r11
      // reserve (4 + 4) words of stack = 8 * 4 = 32
      // r0-r3, r12, lr, pc, xPSR, are pushed automatically
      ".syntax unified\n"
      "mrs  r0, psp\n"          // Get process stack pointer
      // Debug
      "ldr  r1, =exc_context\n"
      "str  r0, [r1]\n"
      "ldr  r1, =current_tcb\n" // Load address of current tcb pointer
      "ldr  r2, [r1]\n"         // Load value of current tcb pointer (first element in the structure is the stack)
      "subs r0, r0, #16\n"      // subtract 32 to account for unsaved registers
      "stmia  r0!, {r4-r7}\n"   // Push r4-r7 on the stack
      "subs r0, #32\n"
      "str  r0, [r2]\n"         // Store the address to complete context in stack pointer in current tcb
      "mov  r4, r8\n"           // Move r8
      "mov  r5, r9\n"           // Move r9
      "mov  r6, r10\n"          // Move r10
      "mov  r7, r11\n"          // Move r11
      "stmia r0!, {r4-r7}\n"    // Push r8-r11 on the task stack
      "push  {r1, lr}\n"        // Push pointer to current tcb and link register on stack
      "cpsid i\n"               // Disable interrupts before running scheduler
      "bl  scheduler\n"
      "cpsie i\n"               // Enable interrupts
      "pop  {r1, r2}\n"       // Pop pointer to current tcb and stacked link register
      "ldr  r0, [r1]\n"       // Get pointer to current tcb
      "ldr  r0, [r0]\n"       // Get address of stack
      "ldmia r0!, {r4-r7}\n"  // Load registers r8-r11
      "mov   r8, r4\n"        // Move r8
      "mov   r9, r5\n"        // Move r9
      "mov   r10, r6\n"       // Move r10
      "mov   r11, r7\n"       // Move r11
      "ldmia r0!, {r4-r7}\n"
      "msr   psp, r0\n"
      // Debug
      "ldr   r1, =exc_context\n" // Load exception frame pointer
      "str   r0, [r1]\n"         // Store exception frame pointer

      "bx    r2\n" // Return from handler

   );
}

void __attribute__((naked)) HardFault_Handler(void) {
   __asm volatile(
      ".syntax unified\n"
      "mov   r0, lr\n"
      "movs  r1, #4\n"
      "tst   r0, r1\n"
      "bne   process_stack\n"
      "mrs   r0, msp\n"
      "b     main_stack\n"
      "process_stack:\n"
      "mrs   r0, psp\n"
      "main_stack:\n"
      "b     hardfault_analyzer\n"
   );
}

void hardfault_analyzer(uint32_t *stack) {
   // Fault stack:
   uint32_t r0 = stack[ 0 ];
   uint32_t r1 = stack[ 1 ];
   uint32_t r2 = stack[ 2 ];
   uint32_t r3 = stack[ 3 ];

   uint32_t r12 = stack[ 4 ];
   uint32_t lr = stack[ 5 ];
   uint32_t pc = stack[ 6 ];
   uint32_t psr = stack[ 7 ];
   while(1);
}