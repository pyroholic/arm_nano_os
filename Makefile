TOOLCHAIN:=/cygdrive/c/Devtools/ARM/7_2017-q4-major/bin/arm-none-eabi-

AS:=$(TOOLCHAIN)as
CC:=$(TOOLCHAIN)gcc
LD:=$(TOOLCHAIN)ld
ASFLAGS:= -mthumb -mcpu=cortex-m0 -g
CCFLAGS:= -mthumb -mcpu=cortex-m0 -g -std=c99 -ffunction-sections -fdata-sections
Q = @

OBJS:= src/crt0.o src/os.o src/os_interrupts.o src/os_handlers.o src/scheduler.o

%.o: %.s Makefile
	@echo "   AS   $@"
	$(Q)$(AS) $(ASFLAGS) -o $@ $<

%.o: %.c Makefile
	@echo "   CC   $@"
	$(Q)$(CC) $(CCFLAGS) -Iinc -c -o $@ $<

os.elf: $(OBJS) Makefile os.ld
	$(CC) -o $@ $(OBJS) -T os.ld -g -Wl,-Map=os.map -Wl,--gc-sections -static -nostartfiles

clean:
	rm $(OBJS)